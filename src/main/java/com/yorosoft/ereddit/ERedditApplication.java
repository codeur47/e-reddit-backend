package com.yorosoft.ereddit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ERedditApplication {
	public static void main(String[] args) {
		SpringApplication.run(ERedditApplication.class, args);
	}
}
