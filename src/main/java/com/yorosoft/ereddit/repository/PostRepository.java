package com.yorosoft.ereddit.repository;

import com.yorosoft.ereddit.model.Post;
import com.yorosoft.ereddit.model.Subreddit;
import com.yorosoft.ereddit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllBySubreddit(Subreddit subreddit);

    List<Post> findByUser(User user);

}
