package com.yorosoft.ereddit.repository;

import com.yorosoft.ereddit.model.Comment;
import com.yorosoft.ereddit.model.Post;
import com.yorosoft.ereddit.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);

}
