package com.yorosoft.ereddit.repository;

import com.yorosoft.ereddit.model.Post;
import com.yorosoft.ereddit.model.User;
import com.yorosoft.ereddit.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
